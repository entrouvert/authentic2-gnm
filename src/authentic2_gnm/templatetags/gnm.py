# authentic2_gnm - Authentic2 plugin for GNM
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import xml.etree.ElementTree as ET

from django import template
from django.conf import settings
from django.utils.encoding import force_str
from django.utils.six.moves.urllib import parse as urlparse

register = template.Library()


@register.filter
def is_coming_for(request):
    if not 'saml:authnRequest' in request.session:
        # look at ?next parameter
        next_url = request.GET.get('next')
    else:
        try:
            authn_request = ET.fromstring(force_str(request.session['saml:authnRequest']))
            next_url = authn_request.findall(
                '{urn:oasis:names:tc:SAML:2.0:protocol}Extensions/{https://www.entrouvert.com/}next_url'
            )[0].text
        except (KeyError, IndexError):
            return 'unknown'
    if not next_url:
        return 'unknown'
    target_path = urlparse.urlparse(next_url).path
    for prefix in ('manage', 'admin', 'backoffice'):
        if target_path.startswith('/%s/' % prefix):
            return 'backoffice'

    target_domain = urlparse.urlparse(next_url).netloc
    if not target_domain:
        # local authentic
        return 'frontoffice'

    if 'agent' in target_domain:
        return 'backoffice'

    target_service = None
    for service_type in settings.KNOWN_SERVICES.keys():
        for service_data in settings.KNOWN_SERVICES[service_type].values():
            if urlparse.urlparse(service_data['url']).netloc == target_domain:
                target_service = service_data
                break
        if target_service:
            if target_service.get('is-portal-agent'):
                return 'backoffice'
            if service_type in ('hobo', 'passerelle', 'welco', 'chrono', 'bijoe'):
                return 'backoffice'
            break

    return 'frontoffice'
