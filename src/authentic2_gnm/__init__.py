# authentic2_gnm - Authentic2 plugin for GNM
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import django.apps
from django.conf import settings


class AppConfig(django.apps.AppConfig):
    name = 'authentic2_gnm'

    def a2_hook_auth_oidc_backend_modify_user(self, user, user_info, **kwargs):
        from authentic2.a2_rbac.models import OrganizationalUnit

        if not hasattr(settings, 'CUT_GNM_OU_MAPPING'):
            return False
        ou_map = {ou.slug: ou for ou in OrganizationalUnit.cached()}
        # move user to the correct organizational unit
        user_ou_slug = settings.CUT_GNM_OU_MAPPING.get(user_info.get('ou'))
        if user_ou_slug:
            user_ou = ou_map.get(user_ou_slug)
            if user_ou and user_ou != user.ou:
                user.ou = user_ou
                return True
        return False


default_app_config = 'authentic2_gnm.AppConfig'


class Plugin:
    def get_before_urls(self):
        return []

    def get_apps(self):
        return [__name__]

    def get_authentication_backends(self):
        return []

    def get_auth_frontends(self):
        return []

    def get_after_middleware(self):
        return []
