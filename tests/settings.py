import os

ALLOWED_HOSTS = ['localhost']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'authentic2-gnm',
    }
}

if 'postgres' in DATABASES['default']['ENGINE']:
    for key in ('PGPORT', 'PGHOST', 'PGUSER', 'PGPASSWORD'):
        if key in os.environ:
            DATABASES['default'][key[2:]] = os.environ[key]

CUT_API_BASE_URL = 'https://cut.base.provider/'
CUT_API_CREDENTIALS = ('abc', 'xyz')

LANGUAGE_CODE = 'en'
A2_FC_CLIENT_ID = ''
A2_FC_CLIENT_SECRET = ''

# test hook handlers
A2_HOOKS_PROPAGATE_EXCEPTIONS = True
