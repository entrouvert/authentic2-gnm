#!/usr/bin/python
import os
import subprocess
import sys
from distutils.command.sdist import sdist

from setuptools import find_packages, setup


class eo_sdist(sdist):
    def run(self):
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        sdist.run(self)
        if os.path.exists('VERSION'):
            os.remove('VERSION')


def get_version():
    if os.path.exists('VERSION'):
        with open('VERSION', 'r') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(
            ['git', 'describe', '--dirty=.dirty', '--match=v*'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        result = p.communicate()[0]
        if p.returncode == 0:
            result = result.decode('ascii').strip()[1:]  # strip spaces/newlines and initial v
            if '-' in result:  # not a tagged version
                real_number, commit_count, commit_hash = result.split('-', 2)
                version = '%s.post%s+%s' % (real_number, commit_count, commit_hash)
            else:
                version = result
            return version
        else:
            return '0.0.post%s' % len(subprocess.check_output(['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0'


setup(
    name='authentic2-gnm',
    version=get_version(),
    license='AGPLv3',
    description='Authentic2 GNM plugin',
    author="Entr'ouvert",
    author_email="info@entrouvert.com",
    packages=find_packages('src'),
    package_dir={
        '': 'src',
    },
    package_data={},
    install_requires=[
        'authentic2',
    ],
    entry_points={
        'authentic2.plugin': [
            'authentic2-gnm = authentic2_gnm:Plugin',
        ],
    },
    cmdclass={'sdist': eo_sdist},
    zip_safe=False,
)
