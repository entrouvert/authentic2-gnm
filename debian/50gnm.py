# disable local registration
REGISTRATION_OPEN = False
A2_EMAIL_IS_UNIQUE = False
A2_AUTH_PASSWORD_ENABLE = True
A2_REGISTRATION_CAN_CHANGE_PASSWORD = True
A2_PROFILE_CAN_CHANGE_EMAIL = False
A2_PROFILE_CAN_EDIT_PROFILE = False
A2_CAN_RESET_PASSWORD = None
A2_REGISTRATION_CAN_DELETE_ACCOUNT = False

# set session age to the CUT "remember me" value
SESSION_COOKIE_AGE = 3600 * 24 * 30

# allow cart@ds technical roles to be provisionned
HOBO_PROVISION_ROLE_PREFIXES = ['_cartads_']

if 'authentic2_gnm' not in INSTALLED_APPS:
    INSTALLED_APPS += ('authentic2_gnm',)
